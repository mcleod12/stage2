
if ($('.js-hero_slick')[0]){
    $('.js-hero_slick').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 500,
        pauseOnFocus:false,
    });
}


if($('.js-product-carousel_slick')[0]){
    $('.js-product-carousel_slick').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
            {
                breakpoint: 1028,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 500,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '24%',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },

            {
                breakpoint: 375,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '20%',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
        ]
    });
}

if($('.js-videos-carousel_slick')[0]){
    $('.js-videos-carousel_slick').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1028,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
        ]
    });
}

if($('#instagram')[0]){
    var accessToken = '388254498.1a8e535.064cc19452b843a4acf4d1d214e88417';
    var num_photos  = 7;
    var feed        = $('#instagram');

    $.ajax({
        type: 'GET',
        url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' + accessToken + '&count=' + num_photos,
        dataType: 'json',
        success: function(result){

            var content = "";
            $.each(result.data, function(i){
                content += i <= 2 || i > 5 ? '<li class="instagram__feed-item">' : '';
                content += i == 2 ? '<ul class="instagram__feed-item__grid">' : '';
                content += i >= 2 && i <= 5 ? '<li class="instagram__feed-item__grid-item">' : '';
                content += ('<a target="_blank" class="instagram__feed-link" href="'+result.data[i].link+'" style="background-image:url('+result.data[i].images.standard_resolution.url+');"></a>');
                content += i >= 2 && i <= 5 ? "</li>" : "";
                content += i == 5 ? "</ul>" : "";
                content += i > 5 ? "</li>" : "";
            });

            feed.append(content);
        }
    });

}

var buttons             = document.querySelectorAll('.subCategories__header-btn'),
    body                = document.querySelectorAll('.subCategories__body'),
    product_holder      = document.querySelectorAll('.subCategories__products-holder'),
    product_hover       = document.querySelectorAll('.productCarousel__product-list-hover'),
    refineResults       = document.querySelectorAll('.filterBlock__item--link'),
    parent              = document.querySelectorAll('.filterBlock__item'),
    clearFilter         = document.querySelectorAll('.filterBlock__link--clearFilter'),
    products            = document.querySelectorAll('.productCarousel__products-link'),
    openButton          = document.querySelectorAll('.productCarousel__products-info-icon'),
    overlay             = document.querySelectorAll('.page__overlay'),
    refineResultsButton = document.querySelectorAll('.js-refineResults'),
    filterBlock         = document.querySelectorAll('.subCategories__body-leftArea'),
    showMoreButton      = document.querySelectorAll('.mediaBlocks__showMore'),
    mediaBlocks         = document.querySelectorAll('.productCarousel__products--mobile'),
    mediaBlockChildren  = mediaBlocks[0].children,
    filter_more         = document.querySelectorAll('.filter_more'),
    filter_items        = document.querySelectorAll('.filterBlock__item');

function displayType(){
    var display_type = this.getAttribute('data-display'),
        section      = document.querySelectorAll('.subCategories__products-holder[data-display="'+display_type+'"]');

    this.classList.add('subCategories__header-btn--active');

    for(var i = 0; i < buttons.length; i++){
        if(buttons[i] !== this){
            buttons[i].classList.remove('subCategories__header-btn--active');
        }
    }

    section[0].style.display = 'block';

    for(var j = 0; j < product_holder.length; j++){
        if(product_holder[j] !== section[0]){
            product_holder[j].style.display = 'none';
        }
    }

    if(display_type === 'list'){
        product_hover[0].setAttribute('style', '');
        body[0].classList.add('subCategories__body--hasRightArea');
    }else{
        product_hover[0].style.display = 'none';
        body[0].classList.remove('subCategories__body--hasRightArea');

    }
}

function refineResultsActive(){
    this.parentNode.classList.add('filterBlock__item--selected');

    for(var i = 0; i < filter_items.length; i++){
        if(filter_items[i] !== this.parentNode){
            filter_items[i].classList.remove('filterBlock__item--selected');
        }
    }
}

// function clearFilter(){
//     $(refineResults).parent(parent).removeClass('filterBlock__item--selected').siblings().removeClass('filterBlock__item--selected');
// }
function handleClick(e){
    e.preventDefault();
}

function productsShow(){

    var container = document.querySelectorAll('.subCategories__products-holder[data-display="list"]');

        if(container[0].style.display === 'block'){
            var children = this.childNodes[3];
            children.style.display = 'block';

            for(var i = 0; i < product_hover.length; i++){
                if(product_hover[i] !== children){
                    product_hover[i].style.display = 'none';
                }
            }
        }else{
            this.childNodes[3].style.display = 'none';
        }

    this.removeEventListener('click', handleClick);
}

function productMouseEnter(){
    var aria_state = this.getAttribute('aria-expanded');

    if(aria_state === "false"){
        this.setAttribute('aria-expanded', 'true');
    }else{
        this.setAttribute('aria-expanded', 'false');
    }
}

function productMouseLeave(){
    var aria_state = this.getAttribute('aria-expanded');

    if(aria_state === "false"){
        this.setAttribute('aria-expanded', 'true');
    }else{
        this.setAttribute('aria-expanded', 'false');
    }
}

function productInfo(){
    var infoBox = this.childNodes[3];

    overlay[0].classList.toggle('page__overlay--is-open');

    if (infoBox.style.display === 'none' || infoBox.style.display === '') {
        infoBox.style.display = 'block';
    } else {
        infoBox.style.display = 'none';
    }
}

function filterShow(){
    filterBlock[0].classList.add('subCategories__body-leftArea--slideOut');
    overlay[0].classList.add('page__overlay--is-open');
}

function filterHide(){
    filterBlock[0].classList.remove('subCategories__body-leftArea--slideOut');
    overlay[0].classList.remove('page__overlay--is-open');
}

function showMore(){
    for(var i = 0; i < mediaBlockChildren.length; i++ ){
        mediaBlockChildren[i].style.display = 'block';
    }
    showMoreButton[0].style.display = 'none';
}

function filterMore(){
    for (var i = 0; i < filter_items.length; i++) {
        filter_items[i].style.display = 'block';
    }
    this.style.display = 'none';
}

if(buttons[0]){
    for(var i = 0; i < buttons.length; i++){
        if (document.addEventListener) {
            buttons[i].addEventListener("click", displayType, false);
        }
    }
}

if(refineResults[0]){
    for(var i = 0; i < refineResults.length; i++){
        if(document.addEventListener){
            refineResults[i].addEventListener("click", refineResultsActive, false);
        }
    }
}

// if(document.addEventListener){
//     for(var i = 0; i < clearFilter.length; i++){
//         clearFilter[i].addEventListener("click", clearFilter, false);
//     }
// }

if(products[0]){
    if($(window).width() <= 1024 && $(window).width() > 600){
        for(var i = 0; i < products.length; i++){
            if(document.addEventListener){
                products[i].addEventListener("click", productsShow, false);
                products[i].addEventListener("click", handleClick, false);
            }
        }
    }

    for(var i = 0; i < products.length; i++){
        if(document.addEventListener){
            products[i].addEventListener("mouseenter", productMouseEnter, false);
            products[i].addEventListener("mouseleave", productMouseLeave, false);

        }
    }

}

if(openButton[0] && $(window).width() <=1200){
    for(var i = 0; i < openButton.length; i++){
        if(document.addEventListener){
            openButton[i].addEventListener("click", productInfo, false);
        }
    }
}

if(refineResultsButton[0]){
    for(var i = 0; i < refineResultsButton.length; i++){
        if(document.addEventListener){
            refineResultsButton[i].addEventListener("click", filterShow, false);
        }
    }
}

if(refineResults[0]){
    for(var i = 0; i < refineResults.length; i++){
        if(document.addEventListener){
            refineResults[i].addEventListener("click", filterHide, false);
        }
    }

    for(var i = 0; i < clearFilter.length; i++){
        if(document.addEventListener){
            clearFilter[i].addEventListener("click", filterHide, false);
        }
    }
}


if(showMoreButton[0] && $(window).width() <= 375){

    for (var j = 0; j < mediaBlockChildren.length; j++) {
        if(j > 2){
            mediaBlockChildren[j].style.display = 'none';
        }
    }

    for(var i = 0; i < showMoreButton.length; i++){
        if(document.addEventListener){
            showMoreButton[i].addEventListener('click', showMore, false);
        }
    }
}


if($('.filterBlock__body')[0]){

    for (var j = 0; j < filter_items.length; j++) {
        if(j > 3){
            filter_items[j].style.display = 'none';
        }
    }
    for(var i = 0; i < filter_more.length; i++){
        if(document.addEventListener){
            filter_more[i].addEventListener('click', filterMore, false);
        }
    }
}