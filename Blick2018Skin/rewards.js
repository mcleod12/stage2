jQuery(document).ready(function($){
    var $table    = $('.rewardsHistory__table'),
        $row      = $table.children('.rewardsHistory__table-row').not('.rewardsHistory__table-row--headings'),
        $showMore = $('.rewardsHistory__showMore');

    $(window).on('load resize', function(){
        if($(window).width() <= 600){

            $row.slice(3).slideUp();
            $showMore.show();

        }else{
            $row.slice(3).slideDown();
        }
    });

    if($showMore[0]){
        $showMore.click(function(){

            if(!$(this).hasClass('showLess')){
                $row.slice(3).slideDown();
                $(this).text('- Show Less').addClass('showLess');
            }else{
                $row.slice(3).slideUp();
                $(this).text('+ Show More').removeClass('showLess');
            }
        });
    }

    $(window).on('load', function(){
        if($(window).width() <= 414){
            var $programDetails = $('.programDetails'),
                $list           = $('.programDetails__list');

            if($list[0]){
                $list.slick({
                    slidesToShow: 1,
                    slideToScroll: 1,
                    infinite: false,
                    adaptiveHeight: true,
                });
            }
        }
    });


    if($('.faqs')[0]){
        var $toggler  = $('.js-faqs_toggler');


        $toggler.click(function(){
            var $listItem = $(this).closest('.faqs__list-item'),
                $body     = $listItem.children('.faqs__list-item--body');

            $body.slideToggle();
            $(this).toggleClass('open');

            if($(this).attr('aria-expanded') === 'false'){
                $(this).attr('aria-expanded', 'true');
            }else{
                $(this).attr('aria-expanded', 'false');
            }
        });
    }


    if($('.js-offer-carousel_slick')[0]){
        $('.js-offer-carousel_slick').slick({
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [
                {
                    breakpoint: 1028,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '24%',
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },

                {
                    breakpoint: 376,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '18%',
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
            ]
        });
    }

});











































































